---
title: "Les fonctions scales"
author: "Maël THEULIERE"
date: '`r Sys.Date()`'
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    self_contained: true
    seal: false
    nature:
      highlightStyle: github
      highlightLines: true
      ratio: 16:9
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(xaringanthemer)
style_duo_accent(
  primary_color = "#1381B0",
  secondary_color = "#FF961C",
  inverse_header_color = "#FFFFFF"
)
```

```{r xaringan-extra, include=FALSE, warning=FALSE}
library(xaringanExtra)
xaringanExtra::use_tachyons()
```

```{r xaringan-extra-styles, include=FALSE, warning=FALSE}
xaringanExtra::use_extra_styles(
  hover_code_line = TRUE,
  mute_unhighlighted_code = TRUE
)
```

```{r xaringanExtra-clipboard, echo=FALSE, eval=TRUE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "<i class=\"fa fa-clipboard\"></i>",
    success_text = "<i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "<i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
library(ggplot2)
library(flair)
library(colorspace)
library(readr)
library(dplyr)
library(scales)
library(start.ggplot)
stringency_index_file <- system.file("extdata/stringency_index.csv", package = "start.ggplot")

stringency_index <- read_csv(stringency_index_file)
```
class: center, middle, title-slide


```{r, include=TRUE,echo=FALSE}
knitr::include_graphics("https://ggplot2.tidyverse.org/logo.png")
```

.white.f1[Une introduction à] .yellow.f1[ggplot2]

.white.f2[Les fonctions] .yellow.f2[scales]

.pull-right[
.white.f3[Maël Theulière, StartR]
]
---
## Configuration

Pour ce tutoriel, nous utilisons les packages suivants.


```{r, eval=FALSE}
library(ggplot2)
library(colorspace)
library(readr)
library(dplyr)
library(scales)
library(readr)
```

Et nous exploitons les données sur la covid 19.

```{r, eval=FALSE}
stringency_index_file <- "extdata/stringency_index.csv"

stringency_index <- read_csv(stringency_index_file)
```
---
## Introduction

Les fonctions **scales** permettent globalement de paramétrer les éléments rentrés dans l'***esthétique*** : 

- Si je veux un gradient de couleur fonction d'une variable continue : quelle palette de couleur je choisie, comment je cale mon dégradé en fonction de cette variable continue ?

- Si je mets une variable continue en ordonnée, comment je définis le minimum et maximum de cette axe, sa position, les valeurs que j'affiche sur l'échelle...
---
## Introduction

L'ensemble des scales possibles peuvent se décrire sous forme suivante:

`scale_xx_yy()`
.pull-left[
ou `xx` peut être un des paramètres de l'aesthétic : 

xx|description
-----------------|---
alpha|transparence
color|couleur des lignes ou des points
fill|couleurs des aires
linetype|type de ligne (continue,pointillée,...)
shape|forme des points
size|aire des points
x|variable de l'axe x
y|variable de l'axe y
]

.pull-right[
Et y un type de paramétrage, comme par exemple :  

yy|description
-----------------|--------------------------
continuous|gérer les variables continue
discrete|gérer les variables discrètes
date|gérer une variable au format date
reverse|inverser l'axe
viridis|utiliser une palette de couleur viridis
manual|définir une palette de couleur via un vecteur de modalités défini par l'utilisateur
]
---
# Introduction

Ci-dessous par exemple, nous allons voir comment changer les couleurs de nos points sur notre graphique type. 

.pull-left[
```{r firstscale,  eval=FALSE}
p <- ggplot(data = penguins) +
  geom_point(aes(
    x = flipper_length_mm,
    y = body_mass_g,
    color = species,
    shape = species
  ),
  size = 2,
  alpha = 0.7
  ) +
  labs(
    title = "Penguin size, Palmer Station LTER",
    subtitle = "Flipper length and body mass for Adelie, Chinstrap and Gentoo Penguins",
    caption = "Gorman KB, Williams TD, Fraser WR (2014)",
    x = "Flipper length (mm)",
    y = "Body mass (g)",
    color = "Penguin species",
    shape = "Penguin species"
  ) +
  theme_minimal()
p
```

]

.pull-right[
```{r firstscale-out, ref.label="firstscale", echo=FALSE, fig.height=4}

```
]
---
# Introduction

Nous allons utiliser `scale_color_manual()` qui permet de modifier les couleurs de notre graphiques points en lui donnant en paramètre un vecteur de couleur de la taille adéquat (ici une couleur pour chaque modalité).

.pull-left[

```{r scale_color_manual, echo=T,eval=F,fig.height=3.5}
p + scale_color_manual(values = c("darkorange", "purple", "cyan4"))
```
]

.pull-right[
```{r scale_color_manual-out, ref.label="scale_color_manual", echo=FALSE, fig.height=4}
```
]
---
class:inverse, center,middle

## Modifier les palettes couleurs
---

## Modifier les palettes couleurs

Avant de modifier les couleurs de vos graphiques, vous aurez à vous poser deux questions importantes : 

### - Quelle couleur je veux remplacer ?
### - Quelle type de variable est colorié ? 

---
## Quelle couleur je veux remplacer ?

Vous avez deux types de couleurs possibles dans ``{ggplot2}` : les couleurs de remplissage ou les couleurs de contours. La première est renseignée avec la paramètre `fill` dans votre esthétique, la deuxième avec le paramètre `color`.

Voyons un exemple sur un diagramme barre : 

.pull-left[

```{r geom_bar,  eval=FALSE}
ggplot(penguins) +
  geom_bar(aes(x = island, fill = species, color = species), alpha = 0.8) +
  theme_minimal() +
  labs(
    x = "Island",
    y = "Frequency",
    title = "Penguin location"
  )
```

]

.pull-right[

```{r geom_bar-out, ref.label="geom_bar", echo=FALSE, fig.height=5}

```

---
## Quelle couleur je veux remplacer ?

`scale_color_manual()` ne va pas dans ce cas changer la couleur de remplissage mais celle des contours de votre graphique : 

.pull-left[

```{r flair-scale_colour_manual, echo = FALSE}
decorate_chunk("scale_colour_manual", eval = FALSE) %>%
  flair(" +
  scale_color_manual(values = c('darkorange', 'purple', 'cyan4'))")
```

]

.pull-right[
```{r scale_colour_manual, echo = FALSE, fig.height=5}
ggplot(penguins) +
  geom_bar(aes(x = island, fill = species, color = species), alpha = 0.8) +
  theme_minimal() +
  labs(
    x = "Island",
    y = "Frequency",
    title = "Penguin location"
  ) +
  scale_color_manual(values = c('darkorange', 'purple', 'cyan4'))
```
]

---
## Quelle couleur je veux remplacer ?


`scale_fill_manual()` vous permettra bien d'obtenir le résultat attendu : 

.pull-left[
```{r flair-scale_fill_manual, echo = FALSE}
decorate_chunk("scale_fill_manual",eval =FALSE) %>%
  flair(" +
  scale_fill_manual(values = c('darkorange', 'purple', 'cyan4'))")
```

]

.pull-right[
```{r scale_fill_manual, echo = FALSE, fig.height=5}
ggplot(penguins) +
  geom_bar(aes(x = island, fill = species, color = species), alpha = 0.8) +
  theme_minimal() +
  labs(
    x = "Island",
    y = "Frequency",
    title = "Penguin location"
  ) +
  scale_color_manual(values = c('darkorange', 'purple', 'cyan4')) +
  scale_fill_manual(values = c('darkorange', 'purple', 'cyan4'))
```

]

---

## Quelle est le type de ma variable ? 

Selon le type de variable que vous voulez colorier, vous devrez utiliser des palettes différentes : 

- Si ma variable est **continue** et représente une **intensité**, par exemple un age moyen, je devrai exploiter un gradient d'intensité.

- Si ma variable est **continue** et possède des valeurs **opposées**, par exemple un taux de croissance avec des valeurs négatives, je devrai exploiter un gradient avec deux couleurs opposées.

- Si ma variable est **discrète** et possède des valeurs avec une **hiérarchie**, je devrai utiliser une palette similaire à mon premier cas.

- Si ma variable est **discrète** et possède des valeurs purement **qualitative**, je devrai utiliser une palette de couleur spécifique permettant de bien discriminer mes différentes modalités.

---
## Quelle est le type de ma variable ? 


Le package [`{colorspace}`](http://colorspace.r-forge.r-project.org/index.html) permet de faciliter l'usage de palettes spécifiques à votre besoin en proposant des palettes types pour ces différents usages et exploite un modèle de couleur HCL (pour teinte-chroma-luminance en anglais) mieux adapté aux différentes déficiences de la vision des couleurs (daltonisme, ...).

```{r, fig.width=10,fig.height=6, dpi=80, echo=FALSE}
hcl_palettes(plot = TRUE)
```
---
## Quelle est le type de ma variable ? 

L'usage de ces palettes dans un graphique `{ggplot2}` est facilité par des fonctions type qui s'appelleront `scale_<aesthetic>_<datatype>_<colorscale>()`, où :

- `<aesthetic>` est le nom de l'aesthetique (*fill*, *color*, *colour*)

- `<datatype>` est le type de variable visualisée  :

  - *discrete* pour une variable discrete 
  - *continuous* pour variable continue, il permet d'obtenir une palette de type carte de chaleur
  - *binned* qui permet de discrétiser une variable continue.

- `<colorscale>` défini le type d'échelle de couleur à utilisée :     

    - *qualitative* : pour une palette de couleur sans hiérarchie spécifique
    - *sequential* pour un palette de couleur basée sur un gradient de valeurs
    - *diverging* ou *divergingx* pour des palettes avec deux couleurs opposées.

Chacune de ces fonctions pourra ensuite prendre en paramètre le nom d'une des palettes prédéfinies.
---

## Quelle est le type de ma variable ? 

Exemple : 

.pull-left[
```{r flair-scale_colour_colorspace, echo = FALSE}
decorate_chunk("scale_colour_colorspace", eval = FALSE) %>%
  flair(" + 
  scale_colour_continuous_sequential(palette = 'Peach')")
```

]

.pull-right[
```{r scale_colour_colorspace, echo = FALSE, fig.height=5}
  ggplot(data = penguins) +
  geom_point(aes(x = bill_length_mm, 
                 y = bill_depth_mm,
                 color = body_mass_g ),
             size = 2.2) +
  theme_minimal() +
  labs(title = "Penguin bill dimensions",
       subtitle = "Bill length and depth for Adelie, Chinstrap and Gentoo Penguins at Palmer Station LTER",
       caption = "Gorman KB, Williams TD, Fraser WR (2014)",
       x = "Bill length (mm)",
       y = "Bill depth (mm)",
       color = "Body mass (g)") + 
  scale_colour_continuous_sequential(palette = 'Peach')
```

]

---

## Un nombre considérable de palettes disponibles

Le nombre de packages permettant d'exploiter des palettes déjà pré-définies est très important. 

[`{paletteer}`](https://emilhvitfeldt.github.io/paletteer/) vise à créer une interface commune à ces différents packages, ce qui en facilite l'usage.

---
## Créer vos palettes de couleur

Si vous souhaitez créer vos propres palettes de couleurs, quelques sites peuvent vous aider pour cela : 

- [Color Space](https://mycolor.space/)

- [Data Color Picker](https://learnui.design/tools/data-color-picker.html)

Si vous souhaiter utiliser vos graphiques en conformité avec la charte graphique de votre organisation, vous pouvez ainsi créer vos palettes de couleurs en partant de cette charte.

---
class: inverse, center, middle
## A vous de jouer

---

## A vous de jouer

Reprendre le graphique suivant vu lors de la première séance, rajouter une couleur d'intensité en fonction du nombre de décès de la covid 19 et choisir une palette de couleur appropriée.

.pull-left[
```{r geom_line_scale_scales, eval = FALSE}
stringency_index %>% 
  filter(CountryName == "France") %>% 
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths), color = 'dark red', size = 1.2) +
  theme_minimal() +
  labs(title = "Nombre de décès liés à la COVID 19 en France",
       y = NULL,
       x = NULL)
```
]

.pull-right[
```{r echo = FALSE, fig.height=5}
stringency_index %>% 
  filter(CountryName == "France") %>% 
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths, color = DailyRollConfirmedDeaths), size = 1.2) +
  theme_minimal() +
  labs(title = "Nombre de décès liés à la COVID 19 en France",
       y = NULL,
       x = NULL,
       color = NULL) +
  scale_colour_binned_sequential(palette = "Red-Blue")
```
]

---
class: inverse, center, middle
## Modifier le formatage des axes

---
class: inverse, center, middle
## les axes continus

---
## Modifier le formatage des axes continus

Le deuxième usage fréquent des fonctions *scales* concerne le formatage des axes.
Plusieurs fonctions vous seront utiles pour cela : 

`scales_x_continuous()` et  `scales_y_continuous()` vous permet de gérer plusieurs paramètres de vos axes X ou Y quand ceux ci représentent des données continues, par exemple : 

- les bornes minimum et maximum
- le format des libellés
- si on souhaite un second axe

---
## Modifier le formatage des axes continus

Exemple sur notre graphique précédent. Nous allons commencé par rajouter un second axe des ordonnées avec le paramètre `sex.axis`. `dup_axis()` signifie qu'on duplique le premier axe des ordonnées. Le paramètre `name = NULL` est spécifié pour ne pas dupliquer le nom de l'axe.


.pull-left[
```{r scale_y_continuous_init,  eval=FALSE}
p <- ggplot(data = penguins) +
  geom_point(aes(
    x = flipper_length_mm,
    y = body_mass_g,
    color = species,
    shape = species),
  size = 2, alpha = 0.7
  ) +
  labs(
    title = "Penguin size, Palmer Station LTER",
    subtitle = "Flipper length and body mass for Adelie, Chinstrap and Gentoo Penguins",
    caption = "Gorman KB, Williams TD, Fraser WR (2014)",
    x = "Flipper length (mm)",
    y = "Body mass (g)",
    color = "Penguin species",
    shape = "Penguin species") +
  theme_minimal() +
  scale_color_manual(values = c("darkorange", "purple", "cyan4"))
p
```

]

.pull-right[
```{r scale_y_continuous_init-out, ref.label="scale_y_continuous_init", echo=FALSE, fig.height=4}

```
]
---

## Modifier le formatage des axes continus

.pull-left[
```{r scale_y_continuous,  eval=FALSE}
p + scale_y_continuous(sec.axis = dup_axis(name = NULL))

```

]

.pull-right[
```{r scale_y_continuous-out, ref.label="scale_y_continuous", echo=FALSE, fig.height=4}

```
]
---
## Modifier le formatage des axes continus


L'instruction `limits` permet de forcer les bornes maximum et minimum. Elle prend en valeur un vecteur des deux bornes souhaitées. Ici on souhaite mettre la valeur par défaut à 0 et garder la valeur maximum par défaut (dans ce cas on met la borne à `NA`).

.pull-left[

```{r flair-scale_y_continuous_limits, echo = FALSE}
decorate_chunk("scale_y_continuous_limits", eval=FALSE) %>%
  flair("limits = c(0, NA),")
```

]

.pull-right[
```{r scale_y_continuous_limits, echo=FALSE, fig.height=5}
p + scale_y_continuous(
  limits = c(0, NA),
  sec.axis = dup_axis(name = NULL)
)
```

]
---
## Modifier le formatage des axes continus

L'instruction `labels` permet de définir une fonction de formatage de nos axes. Pour faciliter cela, on utilisera les fonctions du package `{scales}` qui permettent de formater les variables. Ici on utilise `format_number()` pour lui demander de mettre un séparateur des milliers conforme au système français.

.pull-left[

```{r flair-scale_y_continuous_labels, echo = FALSE}
decorate_chunk("scale_y_continuous_labels", eval = FALSE) %>%
  flair("labels = number_format(big.mark = ' ', suffix = ' g'),")
```

]

.pull-right[

```{r scale_y_continuous_labels, echo=FALSE, fig.height=5}
p + scale_y_continuous(
  labels = number_format(big.mark = ' ', suffix = ' g'),
  limits = c(0, NA),
  sec.axis = dup_axis(name = NULL)
)
```

]


---
## Modifier le formatage des axes continus

`coord_cartesian()` vous permet de gérer plus simplement les limites des deux axes x et y. Il permet également de gérer le positionnement des axes par rapport aux limites définies. Par défaut, `{ggplot2}` ajoute une petite marge pour s'assurer que les données et les axes ne se superpose pas.
Vous pouvez changer cela avec l'option `expand=FALSE`

.pull-left[
```{r coord_cartesian1, eval=FALSE}
p + scale_y_continuous(
  labels = number_format(big.mark = ' '),
  sec.axis = dup_axis(name = NULL)
  ) + 
  coord_cartesian(
    ylim = c(0,NA),
    xlim = c(170,240),
    expand = TRUE
  )
```

]

.pull-right[
```{r coord_cartesian1-out, ref.label="coord_cartesian1", echo=FALSE, fig.height=4}

```

]


---
## Modifier le formatage des axes continus

`coord_cartesian()` vous permet de gérer plus simplement les limites des deux axes x et y. Il permet également de gérer le positionnement des axes par rapport aux limites définies. Par défaut, `{ggplot2}` ajoute une petite marge pour s'assurer que les données et les axes ne se superpose pas.
Vous pouvez changer cela avec l'option `expand=FALSE`

.pull-left[
```{r coord_cartesian2, eval=FALSE}
p + scale_y_continuous(
  labels = number_format(big.mark = ' '),
  sec.axis = dup_axis(name = NULL)
  ) + 
  coord_cartesian(
    ylim = c(0,NA),
    xlim = c(170,240),
    expand = FALSE
  )
```

]

.pull-right[
```{r coord_cartesian2-out, ref.label="coord_cartesian2", echo=FALSE, fig.height=4}

```

]
---
class: inverse, center, middle
## A vous de jouer

---
## A vous de jouer


Reprendre le graphique sur l'évolution des décès liés à la covid 19, et appliquer les mêmes transformations sur l'axe des ordonnées que celles vues sur le graphique précédent pour avoir un deuxième axe à droite et un séparateur des milliers. Utiliser `coord_cartesian()` pour supprimer les marges du panel du graphique. 

En bonus, rajouter une échelle logarithmique sur ce même axe (voir la documentation `?scale_y_continuous`).

.pull-left[
```{r geom_line_scale_y, eval=FALSE}
stringency_index %>% 
  filter(CountryName == "France") %>% 
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths, color = DailyRollConfirmedDeaths), size = 1.2) +
  theme_minimal() +
  labs(title = "Nombre de décès liés à la COVID 19 en France",
       y = NULL,
       x = NULL,
       color = NULL) +
  scale_colour_binned_sequential(palette = "Red-Blue")
```
]
.pull-right[

```{r geom_line_scale_y_solution, echo = FALSE, fig.height=5}
stringency_index %>% 
  filter(CountryName == "France") %>% 
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths, color = DailyRollConfirmedDeaths), size = 1.2) +
  theme_minimal() +
  labs(title = "Nombre de décès liés à la COVID 19 en France",
       y = NULL,
       x = NULL,
       color = NULL) +
  scale_colour_binned_sequential(palette = "Red-Blue") +
  scale_y_continuous(
    labels = number_format(big.mark = " "),
    sec.axis = dup_axis(name = NULL),
    trans = "log10"
  ) + 
  coord_cartesian(
    expand = FALSE
  )
```
]
---
class: inverse,center, middle

## Utiliser les fonctions scales pour ajouter une 2ème echelle
---
## Utiliser les fonctions scales pour ajouter une 2ème echelle

`{ggplot2}` ne permet pas nativement de réaliser un graphique avec deux échelles différentes sur nos axes. 

C'est un choix volontaire de l'auteur du package, dicté par [les nombreuses critiques](https://blog.datawrapper.de/dualaxis/) que l'on peut faire à ce type de graphique.

Toutefois, si vous tenez *vraiment* à réaliser un tel graphique, une utilisation détournée de `sec.axis` vous le permet.

---
## Utiliser les fonctions scales pour ajouter une 2ème echelle

Prenons notre graphique sur les décès liés à la covid 19 en France, auquel nous rajoutons une 2ème série sur le nombre de personnes testées positive.

Ce graphique n'est pas très parlant...

.pull-left[
```{r deces_cas, eval=FALSE}
stringency_index %>%
  filter(CountryName == "France") %>%
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths), color = "dark red", size = 1.2) +
  geom_line(aes(x = Date, y = DailyRollConfirmedCases), color = "#475841", size = 1) +
  theme_minimal() +
  scale_y_continuous(
    labels = number_format(big.mark = " ")
  ) +
  labs(
    title = "COVID 19 : Nombre de décès de personnes testées positive en France",
    y = "Nombre de personnes", x = NULL
  )
```
]

.pull-right[
```{r deces_cas_out, ref.label="deces_cas", echo=FALSE, fig.height=5}

```

]
---
## Utiliser les fonctions scales pour ajouter une 2ème echelle

Nous allons ici tricher en divisant le nombre de cas positifs par 20 et modifier le libellé de notre 2ème axe pour le multiplier par le même nombre.

Notre série de nombre de décès et de nombre de cas ont maintenant chacune leur propre axe des ordonnés.


.pull-left[
```{r deces_cas_triche, eval=FALSE}
stringency_index %>%
  filter(CountryName == "France") %>%
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths), color = "dark red", size = 1.2) +
  geom_line(aes(x = Date, y = DailyRollConfirmedCases/20), color = "#475841", size = 1) +
  theme_minimal() +
  scale_y_continuous(
    labels = number_format(big.mark = " "),
    sec.axis = dup_axis(trans=~.*20, name="Nombre de cas")
  ) +
  labs(
    title = "COVID 19 : Nombre de décès de personnes testées positive en France",
    y = "Nombre de décès", x = NULL
  )
```
]

.pull-right[
```{r deces_cas_triche_out, ref.label="deces_cas_triche", echo=FALSE, fig.height=5}

```

]
---
## Utiliser les fonctions scales pour ajouter une 2ème echelle

Reste un problème : comment savoir à quels axes sont rattachés nos 2 séries ? Pour cela, on peut utiliser le theming par exemple pour modifier la couleur du titre et des libellés de nos axes.


.pull-left[
```{r deces_cas_triche_theme, eval=FALSE}
stringency_index %>%
  filter(CountryName == "France") %>%
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths), color = "dark red", size = 1.2) +
  geom_line(aes(x = Date, y = DailyRollConfirmedCases/20), color = "#475841", size = 1) +
  theme_minimal() +
  theme(
    axis.title.y = element_text(color = "dark red", size=13),
    axis.text.y = element_text(color = "dark red", size=13),
    axis.title.y.right = element_text(color = "#475841", size=13),
    axis.text.y.right = element_text(color = "#475841", size=13)
  ) +
  scale_y_continuous(
    labels = number_format(big.mark = " "),
    sec.axis = dup_axis(trans=~.*20, name="Nombre de cas")
  ) +
  labs(
    title = "COVID 19 : Nombre de décès de personnes testées positive en France", y = "Nombre de décès", x = NULL)
```
]

.pull-right[
```{r deces_cas_triche_theme_out, ref.label="deces_cas_triche_theme", echo=FALSE, fig.height=5}

```

]

---
class: inverse,center, middle

## Modifier le formatage des axes date
---
## Modifier le formatage des axes date

`scales_x_date()` et `scales_y_date()` vous permettent de modifier le formatage de vos axes dates. Deux paramètres important vous seront utiles sur ces axes, `date_labels` et `date_breaks` modifient respectivement le libellé des dates et pas de temps qui définis les grilles.

Reprenons notre graphique sur l'évolution des décès liés à la covid 19.

.pull-left[
```{r scale_x_date_1, eval=FALSE}
stringency_index %>% 
  filter(CountryName == "France") %>% 
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths), size = 1.2, color = "dark red") +
  theme_minimal() +
  labs(title = "Nombre de décès liés à la COVID 19 en France",
       y = NULL,
       x = NULL,
       color = NULL)
```

]
.pull-right[
```{r scale_x_date_1-out, ref.label="scale_x_date_1", echo=FALSE, fig.height=4}

```
]
---
## Modifier le formatage des axes date

`scales_x_date()` et `scales_y_date()` vous permettent de modifier le formatage de vos axes dates. Deux paramètres important vous seront utiles sur ces axes, `date_labels` et `date_breaks` modifient respectivement le libellé des dates et pas de temps qui défini les grilles.

Reprenons notre graphique sur l'évolution des décès liés à la covid 19.

Ici nous modifions le pas des date à 3 mois et mettons le libellé sous un format avec le nom du mois abrégé et l'année en 4 chiffres. Pour connaître toutes les écritures possibles d'une date, vous pouvez consulter [cette page](https://stat.ethz.ch/R-manual/R-devel/library/base/html/strptime.html).

.pull-left[
```{r scale_x_date_2, eval=FALSE}
stringency_index %>% 
  filter(CountryName == "France") %>% 
  ggplot() +
  geom_line(aes(x = Date, y = DailyRollConfirmedDeaths), size = 1.2, color = "dark red") +
  theme_minimal() +
  labs(title = "Nombre de décès liés à la COVID 19 en France",
       y = NULL,
       x = NULL,
       color = NULL) + 
  scale_x_date(date_breaks = "3 months",
               date_labels = "%b %Y")
```

]
.pull-right[
```{r scale_x_date_2-out, ref.label="scale_x_date_2", echo=FALSE, fig.height=4}

```
]
